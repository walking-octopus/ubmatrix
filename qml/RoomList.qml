import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import Ubuntu.Components.Themes 1.3
import Matrix 1.0

Rectangle {
    color: theme.palette.normal.base

    signal enterRoom(var room)
    signal joinRoom(string name)

    property bool initialised: false

    RoomListModel {
        id: rooms
    }

    function setConnection(conn) {
        rooms.setConnection(conn)
    }

    function init() {
        initialised = true
        var found = false
        for(var i = 0; i < rooms.rowCount(); i++) {
            if(rooms.roomAt(i).canonicalAlias() === "#tensor:matrix.org") {
                roomListView.currentIndex = i
                enterRoom(rooms.roomAt(i))
                found = true
            }
        }
        if(!found) joinRoom("#tensor:matrix.org")
    }

    function refresh() {
        if(roomListView.visible)
            roomListView.forceLayout()
    }

    Column {
        anchors.fill: parent

        ListView {
            id: roomListView
            model: rooms
            width: parent.width
            height: parent.height - textEntry.height

            delegate: Rectangle {
                width: parent.width
                height: Math.max(units.gu(4), roomLabel.implicitHeight + units.gu(0.5))
                color: "transparent"

                Label {
                    id: roomLabel
                    text: display
                    color: theme.palette.normal.baseText
                    elide: Text.ElideRight
                    font.bold: roomListView.currentIndex == index

                    anchors {
                        margins: units.gu(0.25)
                        leftMargin: units.gu(0.75)
                        left: parent.left
                        right: parent.right
                        verticalCenter: parent.verticalCenter
                    }
                }

                MouseArea {
                    anchors.fill: parent
                    onPressed: {
                        roomListView.currentIndex = index
                        enterRoom(rooms.roomAt(index))
                    }
                }
            }

            highlight: Rectangle {
                height: units.gu(2.5)
                radius: 2
                color: theme.palette.normal.highlighted
            }

            onCountChanged: if(initialised) {
                roomListView.currentIndex = count-1
                enterRoom(rooms.roomAt(count-1))
            }
        }

        TextField {
            id: textEntry
            width: parent.width
            placeholderText: qsTr("Join room...")
            onAccepted: { joinRoom(text); text = "" }
        }
    }
}
