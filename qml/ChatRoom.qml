import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import Matrix 1.0

Rectangle {
    id: root

    property Connection currentConnection: null
    property var currentRoom: null

    function setRoom(room) {
        currentRoom = room
        messageModel.changeRoom(room)
    }

    function setConnection(conn) {
        currentConnection = conn
        messageModel.setConnection(conn)
    }

    function sendLine(text) {
        if(!currentRoom || !currentConnection) return
        currentConnection.postMessage(currentRoom, "m.text", text)
    }

    ListView {
        id: chatView
        anchors.fill: parent
        model: MessageEventModel { id: messageModel }

        flickableDirection: Flickable.VerticalFlick
        verticalLayoutDirection: ListView.BottomToTop

        delegate: Row {
            id: message
            width: parent.width
            anchors.margins: units.gu(0.5)
            spacing: units.gu(1)

            Label {
                id: timelabel
                text: time.toLocaleTimeString("hh:mm:ss")
                color: "grey"
            }

            Text {
                text: eventType == "message" ? author : "***"
                width: units.gu(8)
                color: eventType == "message" ? "grey" : "lightgrey"

                // elide: Text.ElideRight
                horizontalAlignment: Text.AlignRight
                renderType: Text.NativeRendering
                textFormat: TextEdit.RichText
            }

            Text {
                text: content
                width: parent.width - (x - parent.x) - spacing
                color: eventType == "message" ? "black" : "lightgrey"

                renderType: Text.NativeRendering
                textFormat: TextEdit.RichText
                wrapMode: Text.Wrap
            }
        }

        section {
            property: "date"
            labelPositioning: ViewSection.CurrentLabelAtStart

            delegate: Rectangle {
                width: parent.width
                height: childrenRect.height

                Label {
                    width: parent.width
                    text: section.toLocaleString(Qt.locale())
                    color: "grey"
                    horizontalAlignment: Text.AlignRight
                }
            }
        }

        onAtYBeginningChanged: {
            if(!!currentRoom && atYBeginning)
                currentRoom.getPreviousContent()
        }
    }
}
