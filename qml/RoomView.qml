import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

Item {
    id: room

    function setRoom(room) {
        chat.setRoom(room)
    }

    function setConnection(conn) {
        chat.setConnection(conn)
    }

    function sendLine(line) {
        chat.sendLine(line)
        textEntry.text = ''
    }

    ChatRoom {
        id: chat

        anchors {
            bottom: textEntry.top
            right: parent.right
            left: parent.left
            top: parent.top
        }
    }

    TextField {
        id: textEntry
        anchors {
            right: parent.right
            left: parent.left
            bottom: parent.bottom
        }

        focus: true
        //textColor: "black"
        placeholderText: qsTr("Say something...")
        onAccepted: sendLine(text)
    }
}
