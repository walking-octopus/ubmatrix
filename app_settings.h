#ifndef AppSettings_H
#define AppSettings_H

#include <QObject>
#include <QSettings>

class AppSettings : public QSettings
{
    Q_OBJECT
public:
    AppSettings(QObject *parent = NULL);
    virtual ~AppSettings();

    Q_INVOKABLE void setValue(const QString &key, const QVariant &value);
    Q_INVOKABLE QVariant value(const QString &key, const QVariant &defaultValue = QVariant()) const;
};

#endif // AppSettings_H
